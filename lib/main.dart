import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    getData();
  }

  Future getData() async {
    var url =
        Uri.parse('https://coderbyte.com/api/challenges/json/json-cleaning');

    var response = await http.get(url);

    Map convertedJsonData = jsonDecode(response.body);

    validateJson(convertedJsonData);
  }

  validateJson(Map jsonMap) {

    jsonMap.removeWhere((key, value) {
      if (value is List) {
        //Removing Key's & Values from this List
        value.removeWhere((value) =>  value == '-' || value == '' || value == 'N/A');
      }
      if (value is Map) {
        //Removing Key's & Values from this Map
        value.removeWhere((key, value) => value == '-' || value == '' || value == 'N/A');
      }
      return value == '-' || value == '' || value == 'N/A';
    });

    print(jsonMap);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
          child: Container(
            child: Text('Hello World'),
          ),
        ),
      ),
    );
  }
}
